import {MigrationInterface, QueryRunner} from "typeorm";

export class addDeleteAt1661154414011 implements MigrationInterface {
    name = 'addDeleteAt1661154414011'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`user\` ADD \`delete_at\` datetime(6) NULL`);
        await queryRunner.query(`ALTER TABLE \`post\` ADD \`delete_at\` datetime(6) NULL`);
        await queryRunner.query(`ALTER TABLE \`comment\` ADD \`delete_at\` datetime(6) NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`comment\` DROP COLUMN \`delete_at\``);
        await queryRunner.query(`ALTER TABLE \`post\` DROP COLUMN \`delete_at\``);
        await queryRunner.query(`ALTER TABLE \`user\` DROP COLUMN \`delete_at\``);
    }

}
