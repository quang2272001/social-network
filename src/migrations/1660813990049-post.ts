import { MigrationInterface, QueryRunner } from 'typeorm';

export class post1660813990049 implements MigrationInterface {
  name = 'post1660813990049';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE \`post\` ADD \`created_at\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6)`,
    );
    await queryRunner.query(
      `ALTER TABLE \`post\` ADD \`updated_at\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6)`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE \`post\` DROP COLUMN \`updated_at\``);
    await queryRunner.query(`ALTER TABLE \`post\` DROP COLUMN \`created_at\``);
  }
}
