import {MigrationInterface, QueryRunner} from "typeorm";

export class addDefaultForLike1661168599651 implements MigrationInterface {
    name = 'addDefaultForLike1661168599651'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`post\` CHANGE \`like\` \`like\` int NOT NULL DEFAULT '0'`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`post\` CHANGE \`like\` \`like\` int NOT NULL`);
    }

}
