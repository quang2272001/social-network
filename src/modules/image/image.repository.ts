import { NotFoundException } from '@nestjs/common';
import { EntityRepository, Repository } from 'typeorm';
import { IPost } from '../post/dtos/post.dto';
import { Image } from './image.entity';

@EntityRepository(Image)
export class ImageRepository extends Repository<Image> {
  async addImage(filePaths: string[], post: IPost) {
    const images = await this.save(
      filePaths.map((path) => this.create({ path, post })),
    );
    return images.map(({ id, path, created_at, updated_at }) => {
      return { id, path, created_at, updated_at };
    });
  }
  async getImage(imageId: number) {
    const image = await this.findOne(imageId);
    if (!image) throw new NotFoundException('image not found');
    const { id, path, created_at, updated_at } = image;
    return { id, path, created_at, updated_at };
  }
  async editImage(id: number, filePath: string) {
    const image = await this.findOne(id);
    if (!image) throw new NotFoundException('Image not found');
    await this.update(id, { path: filePath });
  }
  async deleteImage(id: number) {
    const image = await this.findOne(id);
    if (!image) throw new NotFoundException('Image not found');
    await this.softDelete(id);
  }
}
