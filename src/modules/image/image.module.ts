import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ImageController } from './image.controller';
import { ImageRepository } from './image.repository';
import { ImageService } from './image.service';

@Module({
  controllers: [ImageController],
  imports: [TypeOrmModule.forFeature([ImageRepository])],
  providers: [ImageService],
  exports: [ImageService],
})
export class ImageModule {}
