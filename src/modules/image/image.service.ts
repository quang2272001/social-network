import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { IPost } from '../post/dtos/post.dto';
import { ImageRepository } from './image.repository';

@Injectable()
export class ImageService {
  constructor(
    @InjectRepository(ImageRepository) private imageRepository: ImageRepository,
  ) {}
  async addImage(filePaths: string[], post: IPost) {
    return this.imageRepository.addImage(filePaths, post);
  }
  async getImage(imageId: number) {
    return this.imageRepository.getImage(imageId);
  }
}
