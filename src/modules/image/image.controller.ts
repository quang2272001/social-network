import { Controller, Get, Param, Res, StreamableFile } from '@nestjs/common';
import { Response } from 'express';
import { createReadStream } from 'fs';
import { join } from 'path';
import { ValidateId } from 'src/util/dtos/validateParam.dto';
import { ImageService } from './image.service';

@Controller()
export class ImageController {
  constructor(private imageService: ImageService) {}
  @Get(':id')
  async getImage(
    @Param() param: ValidateId,
    @Res({
      passthrough: true,
    })
    res: Response,
  ) {
    const image = await this.imageService.getImage(param.id);
    const file = createReadStream(image.path);
    res.set({
      'Content-Type': 'image/png',
      'Content-Disposition':
        'attachment; filename="1661420397715-database diagram.png"',
    });
    return new StreamableFile(file);
  }
}
