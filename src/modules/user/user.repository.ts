import { HttpErrorByCode } from '@nestjs/common/utils/http-error-by-code.util';
import { EntityRepository, Repository } from 'typeorm';
import { User } from './user.entity';
@EntityRepository(User)
export class UserRepository extends Repository<User> {
  async getUserByEmail(email: string) {
    const user = await this.findOne({ where: { email: email } });
    if (!user) throw new HttpErrorByCode[404]('User not Found');
    const { password, id, name, createAt } = user;
    return {
      password,
      id,
      name,
      createAt,
      email,
    };
  }
  async getUserById(userId: number) {
    const user = await this.findOne(userId);
    if (!user) throw new HttpErrorByCode[404]('User not Found');
    const { id, name, createAt } = user;
    return {
      id,
      name,
      createAt,
    };
  }
}
