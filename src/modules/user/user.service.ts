import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateUserDto } from './dtos/user.dto';
import { UserRepository } from './user.repository';
@Injectable()
export class UserService {
  constructor(
    @InjectRepository(UserRepository) private userRepository: UserRepository,
  ) {}
  async signup(createUserDto: CreateUserDto) {
    return this.userRepository.save(createUserDto);
  }
  async getUserByEmail(email: string) {
    return await this.userRepository.getUserByEmail(email);
  }
  async getUserById(userId: number) {
    return this.userRepository.getUserById(userId);
  }
}
