export interface IUser {
  id: number;
  name: string;
  createAt: Date;
}
