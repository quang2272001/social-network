import { IsNotEmpty, IsString } from 'class-validator';
import { CreatePortDto } from 'src/modules/post/dtos/post.dto';

export class CreateCommentDto {
  @IsNotEmpty()
  @IsString()
  content: string;
}
export interface IComment {
  id: number;
  content: string;
  like: number;
  post: CreatePortDto;
}
