import { Body, Controller, Post } from '@nestjs/common';
import { PostService } from 'src/modules/post/post.service';
import { CommentService } from './comment.service';
import { CreateCommentDto } from './dtos/comment.dtos';

@Controller('comment')
export class CommentController {
  constructor(private commentService: CommentService) {}
}
