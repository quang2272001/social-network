import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CommentController } from './comment.controller';
import { CommentRepository } from './comment.repository';
import { CommentService } from './comment.service';

@Module({
  controllers: [CommentController],
  imports: [TypeOrmModule.forFeature([CommentRepository])],
  providers: [CommentService],
})
export class CommentModule {}
