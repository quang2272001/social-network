import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { IPost } from 'src/modules/post/dtos/post.dto';
import { IUser } from '../user/interface/user.interface';
import { CommentRepository } from './comment.repository';
import { CreateCommentDto } from './dtos/comment.dtos';

@Injectable()
export class CommentService {
  constructor(
    @InjectRepository(CommentRepository)
    private commentRepository: CommentRepository,
  ) {}
  async createComment(post: IPost, commentData: CreateCommentDto, user: IUser) {
    return this.commentRepository.createComment(post, commentData, user);
  }
}
