import { CreatePortDto, IPost } from 'src/modules/post/dtos/post.dto';
import { EntityRepository, Repository } from 'typeorm';
import { IUser } from '../user/interface/user.interface';
import { Comment } from './comment.entity';
import { CreateCommentDto } from './dtos/comment.dtos';

@EntityRepository(Comment)
export class CommentRepository extends Repository<Comment> {
  async createComment(
    post: IPost,
    commentData: CreateCommentDto,
    createUser: IUser,
  ) {
    const { id, content, like, created_at, delete_at, updated_at, user } =
      await this.save({
        ...commentData,
        post,
        user: createUser,
      });
    return { id, content, like, created_at, delete_at, updated_at, user };
  }
}
