import { Allow, IsNotEmpty, IsNumber, IsString, Length } from 'class-validator';
import { IComment } from 'src/modules/comment/dtos/comment.dtos';
import { IUser } from 'src/modules/user/interface/user.interface';
export class CreatePortDto {
  @IsNotEmpty()
  @IsString()
  @Length(3)
  title: string;
  @IsNotEmpty()
  @IsString()
  content: string;
  @Allow()
  @IsNumber()
  like = 0;
}
export class UpdatePortDto {
  @IsNotEmpty()
  @IsString()
  @Length(3)
  title: string;
  @IsNotEmpty()
  @IsString()
  content: string;
}
export interface IPost {
  id: number;
  title: string;
  content: string;
  like: number;
  created_at: Date;
  updated_at: Date;
  user: IUser;
}
