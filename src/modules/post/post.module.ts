import { Module } from '@nestjs/common';
import { MulterModule } from '@nestjs/platform-express';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CommentRepository } from 'src/modules/comment/comment.repository';
import { CommentService } from 'src/modules/comment/comment.service';
import { UserModule } from '../user/user.module';
import { PostController } from './post.controller';
import { PostRepository } from './post.repository';
import { PostService } from './post.service';
import { diskStorage } from 'multer';
import { ImageModule } from '../image/image.module';
@Module({
  controllers: [PostController],
  imports: [
    TypeOrmModule.forFeature([PostRepository, CommentRepository]),
    MulterModule.register({
      // dest: process.cwd() + '/uploads',
    }),
    UserModule,
    ImageModule,
  ],
  providers: [PostService, CommentService],
})
export class PostModule {}
