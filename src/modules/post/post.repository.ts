import { HttpErrorByCode } from '@nestjs/common/utils/http-error-by-code.util';
import { EntityRepository, Repository } from 'typeorm';
import { IUser } from '../user/interface/user.interface';
import { CreatePortDto, UpdatePortDto } from './dtos/post.dto';
import { Post } from './post.entity';
@EntityRepository(Post)
export class PostRepository extends Repository<Post> {
  async createPost(createUser: IUser, postData: CreatePortDto) {
    const { id, title, content, like, created_at, updated_at, user } =
      await this.save(this.create({ ...postData, user: createUser }));
    return { id, title, content, like, created_at, updated_at, user };
  }
  async getListPost() {
    const listPost = await this.find({
      relations: ['comments'],
    });
    return listPost.map(({ id, title, content, like }) => {
      return { id, title, content, like };
    });
  }
  async getPost(postId: number) {
    // dont using query builder
    // const checkPost = await this.findOne({
    //   where: { id: postId },
    //   relations: ['user'],
    //   loadRelationIds: true,
    // });

    // using query builder
    const checkPost = await this.createQueryBuilder('post')
      .leftJoin('post.user', 'user')
      .addSelect(['user.name', 'user.id', 'user.createAt'])
      .where('post.id = :postId', { postId })
      .getOne();
    if (!checkPost) throw new HttpErrorByCode[404]('Post not found');
    const { id, content, like, title, created_at, updated_at, user } =
      checkPost;
    return { id, content, like, title, created_at, updated_at, user };
  }
  async deletePost(postId: number) {
    const checkPost = await this.findOne(postId);
    if (!checkPost) throw new HttpErrorByCode[404]('Post not found');
    await this.softDelete(postId);
    return {
      success: true,
      message: 'successfully delete post',
    };
  }
  async updatePost(postId: number, postData: UpdatePortDto) {
    await this.update(postId, postData);
    return {
      success: true,
      message: 'successfully update post',
    };
  }
}
