import { Injectable, Logger } from '@nestjs/common';
import { HttpErrorByCode } from '@nestjs/common/utils/http-error-by-code.util';
import { InjectRepository } from '@nestjs/typeorm';
import { IUser } from '../user/interface/user.interface';
import { CreatePortDto, UpdatePortDto } from './dtos/post.dto';
import { PostRepository } from './post.repository';

@Injectable()
export class PostService {
  constructor(
    @InjectRepository(PostRepository) private postRepository: PostRepository,
  ) {}
  // using logger
  private readonly logger = new Logger(PostService.name);

  async createPost(user: IUser, postData: CreatePortDto) {
    return await this.postRepository.createPost(user, postData);
  }
  async getListPost() {
    this.logger.log('ok');
    return await this.postRepository.getListPost();
  }
  async updatePost(postId: number, user: IUser, updateData: UpdatePortDto) {
    const checkPost = await this.getPost(postId);
    if (!checkPost) throw new HttpErrorByCode[404]('Post not found');
    if (JSON.stringify(user) !== JSON.stringify(checkPost.user))
      throw new HttpErrorByCode['403']();
    return this.postRepository.updatePost(postId, updateData);
  }
  async getPost(postId: number) {
    return await this.postRepository.getPost(postId);
  }
  async deletePost(postId: number) {
    return await this.postRepository.deletePost(postId);
  }
}
