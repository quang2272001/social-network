import { Comment } from '../comment/comment.entity';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { User } from '../user/user.entity';
import { Image } from '../image/image.entity';
@Entity()
export class Post {
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  title: string;
  @Column()
  content: string;
  @Column({ default: 0 })
  like: number;
  @CreateDateColumn()
  created_at: Date;
  @CreateDateColumn()
  updated_at: Date;
  @DeleteDateColumn()
  delete_at: Date;
  @ManyToOne(() => User, (user) => user.posts)
  user: User;
  @OneToMany(() => Comment, (comment) => comment.post, { cascade: true })
  comments: Comment;
  @OneToMany(() => Image, (image) => image.post, { cascade: true })
  images: Image;
}
