import {
  BadRequestException,
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  Param,
  Post,
  Put,
  Request,
  Res,
  StreamableFile,
  UploadedFile,
  UploadedFiles,
  UseGuards,
  UseInterceptors,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { HttpErrorByCode } from '@nestjs/common/utils/http-error-by-code.util';
import { AuthGuard } from '@nestjs/passport';
import {
  FileFieldsInterceptor,
  FileInterceptor,
  FilesInterceptor,
} from '@nestjs/platform-express';
import { Response } from 'express';
import { createReadStream, createWriteStream } from 'fs';
import { diskStorage } from 'multer';
import { join } from 'path';
import { CommentService } from 'src/modules/comment/comment.service';
import { CreateCommentDto } from 'src/modules/comment/dtos/comment.dtos';
import { ValidateId } from '../../util/dtos/validateParam.dto';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { ImageService } from '../image/image.service';
import { IUser } from '../user/interface/user.interface';
import { UserService } from '../user/user.service';
import { CreatePortDto, IPost, UpdatePortDto } from './dtos/post.dto';
import { PostService } from './post.service';
@UseGuards(JwtAuthGuard)
@Controller('post')
export class PostController {
  constructor(
    private postService: PostService,
    private commentService: CommentService,
    private userService: UserService,
    private imageService: ImageService,
  ) {}
  @Post()
  @HttpCode(200)
  // upload file by multiple filed
  // @UseInterceptors(FileFieldsInterceptor([{ name: 'avatar', maxCount: 1 },{ name: 'background', maxCount: 1 }]))
  @UseInterceptors(
    FilesInterceptor('file', 5, {
      fileFilter: (req, file, callback) => {
        if (!file.originalname.match(/\.(jpg|jepg|png|gif)$/))
          return callback(
            new BadRequestException('You can upload image'),
            false,
          );
        callback(null, true);
      },
      limits: { fileSize: 10000000 },
      storage: diskStorage({
        destination: process.cwd() + '/uploads',
        // set file name
        filename: function (req, file, callback) {
          callback(null, Date.now() + '-' + file.originalname);
        },
      }),
    }),
  )
  @UsePipes(ValidationPipe)
  async createPost(
    @Body() createData: CreatePortDto,
    @Request() req,
    @UploadedFiles() images: Array<Express.Multer.File>,
  ) {
    const user: IUser = await this.userService.getUserById(req.user.sub);
    const post = await this.postService.createPost(user, createData);
    if (images)
      await this.imageService.addImage(
        images.map((image) => image.path),
        post,
      );
    return 'ok';
  }
  // upload a array file in a field
  // @Post('upload')
  // @UseInterceptors(FilesInterceptor('files'))
  // uploadFile(@UploadedFiles() files: Array<Express.Multer.File>) {
  //   console.log(files);
  // }
  @Get()
  @HttpCode(200)
  async getListPost() {
    return await this.postService.getListPost();
  }
  @Get(':id')
  @UsePipes(ValidationPipe)
  @HttpCode(200)
  async getPost(@Param() param: ValidateId) {
    return await this.postService.getPost(param.id);
  }
  @UsePipes(ValidationPipe)
  @Post(':id/comment/')
  @HttpCode(201)
  async addComment(
    @Param() param: ValidateId,
    @Body() commentData: CreateCommentDto,
    @Request() req,
  ) {
    const post: IPost = await this.postService.getPost(param.id);
    if (!post) return new HttpErrorByCode[404]('Post not found');
    const createUser = await this.userService.getUserById(req.user.sub);
    if (!createUser) return new HttpErrorByCode[404]('User not found');
    const comment = await this.commentService.createComment(
      post,
      commentData,
      createUser,
    );
    return {
      status: 200,
      data: comment,
    };
  }
  @Put(':id')
  @HttpCode(200)
  @UsePipes(ValidationPipe)
  async updatePost(
    @Param() param: ValidateId,
    @Body() updateData: UpdatePortDto,
    @Request() req,
  ) {
    const user: IUser = await this.userService.getUserById(req.user.sub);
    return await this.postService.updatePost(param.id, user, updateData);
  }
  @Delete(':id')
  @HttpCode(200)
  @UsePipes(ValidationPipe)
  async deletePost(@Param() param: ValidateId) {
    return this.postService.deletePost(param.id);
  }
}
