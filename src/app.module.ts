import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PostModule } from './modules/post/post.module';
import typeormConfig from './config/typeorm.config';
import { UserModule } from './modules/user/user.module';
import { CommentModule } from './modules/comment/comment.module';
import { AuthModule } from './modules/auth/auth.module';
import { ImageModule } from './modules/image/image.module';
@Module({
  imports: [
    TypeOrmModule.forRoot(typeormConfig),
    PostModule,
    UserModule,
    CommentModule,
    AuthModule,
    ImageModule,
  ],
})
export class AppModule {}
