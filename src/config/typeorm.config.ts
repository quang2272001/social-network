import { TypeOrmModuleOptions } from '@nestjs/typeorm';

export default {
  type: 'mysql',
  host: 'localhost',
  port: 3307,
  username: 'root',
  password: 'matkhaumanh',
  database: 'test',
  entities: ['dist/**/*.entity{.ts,.js}'],
  migrations: ['dist/migrations/*.js'],
  synchronize: false,
  cli: {
    migrationsDir: 'src/migrations',
  },
  logging: true,
} as TypeOrmModuleOptions;
